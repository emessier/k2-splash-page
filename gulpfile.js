var gulp = require('gulp');
var livereload = require('gulp-livereload')
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var rename = require('gulp-rename');

minifyCss = require('gulp-minify-css'); //minifies css
concat = require('gulp-concat'); //joins multiple files into one

gulp.task('imagemin', function() {
  return gulp.src('assets/images/*')
    .pipe(imagemin({
      progressive: true,
      use: [pngquant()]
    }))
    .pipe(gulp.dest('assets/dist/img'));
});

gulp.task('css', function() {
  gulp.src('assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer(
      "Android 2.3",
      "Android >= 4",
      "Chrome >= 20",
      "Firefox >= 24",
      "Explorer >= 8",
      "iOS >= 6",
      "Opera >= 12",
      "Safari >= 6"
    ))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('assets/dist/css'));
});

gulp.task('minifyCss', function() {
  gulp.src('assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(autoprefixer(
      "Android 2.3",
      "Android >= 4",
      "Chrome >= 20",
      "Firefox >= 24",
      "Explorer >= 8",
      "iOS >= 6",
      "Opera >= 12",
      "Safari >= 6"
    ))
    .pipe(minifyCss())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('assets/dist/css'));
});

gulp.task('uglify', function() {
  gulp.src('assets/js/*.js')
    .pipe(uglify('main.js'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('assets/dist/js'));
});

gulp.task('watch', function() {
  livereload.listen();

  gulp.watch('assets/scss/**/*.scss', ['css']);
  gulp.watch('assets/scss/**/*.scss', ['minifyCss']);
  gulp.watch('assets/js/main.js', ['uglify']);
  gulp.watch(['assets/dist/css/*.css', 'assets/dist/js/main.min.js'], function(files) {
    livereload.changed(files)
  });
});
